﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class ImportObjWindow : Form
    {
        int ii = 0;
        int jj = 0;
        int rows = 0;

        public ImportObjWindow()
        {
            InitializeComponent();
            pictable.AllowDrop = true;
            pictable.DragDrop += new DragEventHandler(pict_DragDrop);
            pictable.DragEnter += new DragEventHandler(pict_DragEnter);

            pictable.RowStyles.Clear();  //first you must clear rowStyles
        }

        private void ImportWindow_Load(object sender, EventArgs e)
        {

        }




        private void pict_DragDrop(object sender, DragEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Graphics g = picbox.CreateGraphics();
            g.DrawImage((Image)e.Data.GetData(DataFormats.Bitmap), new Point(0, 0));
        }

        private void pict_DragEnter(object sender, DragEventArgs e)
        {

                e.Effect = DragDropEffects.Copy;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < 5; i++)
            {
                PictureBox p1 = new PictureBox();
                p1.Dock = DockStyle.Fill;
                p1.SizeMode = PictureBoxSizeMode.StretchImage;
                p1.Name = "picbox" + ii;

                pictable.Controls.Add(p1, i, rows);
                ii++;
            }

            pictable.RowStyles.Add(new RowStyle(SizeType.Absolute, 160)); // 30 is the rows space
            

            for (int j = 0; j < 5; j++)
            {
                Panel n1 = new Panel();
                n1.Dock = DockStyle.Fill;

                Button b1 = new Button();
                b1.Name = "button" + jj;
                b1.Text = "Load";
                b1.Click += new EventHandler(ButtonClickOneEvent);
                b1.Tag = jj;
                n1.Controls.Add(b1);

                TextBox t1 = new TextBox();
                t1.Height = 20;
                t1.Width = 30;
                t1.Dock = DockStyle.Right;
                t1.Name = "label" + jj;
                n1.Controls.Add(t1);
                pictable.Controls.Add(n1, j, rows + 1);
                jj++;
            }
            rows = rows + 2;
            pictable.RowStyles.Add(new RowStyle(SizeType.Absolute, 30)); // 30 is the rows space
        }

        void ButtonClickOneEvent(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    PictureBox pictureBox1 = (PictureBox)this.Controls.Find("picbox" + button.Tag, true)[0];
                    pictureBox1.Load(openFileDialog1.FileName);
                }
            }
        }

    }
}
