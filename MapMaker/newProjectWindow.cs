﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class newProjectWindow : Form
    {
        Map _map;

        public newProjectWindow(ref Map map)
        {
            InitializeComponent();
            _map = map;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public delegate void makeGrid();

        private void createButton_Click(object sender, EventArgs e)
        {
            _map.Initialize(Convert.ToInt32(tileBoxX.Text),
                Convert.ToInt32(tileBoxY.Text),
                Convert.ToInt32(mapBoxX.Text),
                Convert.ToInt32(mapBoxY.Text),
                mapNameBox.Text);

            (System.Windows.Forms.Application.OpenForms["Form1"] as Form1).makeGrid();

            this.Close();
        }
    }
}
