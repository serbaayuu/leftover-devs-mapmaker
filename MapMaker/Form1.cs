﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;

namespace TestProgram
{
    public partial class Form1 : Form
    {
        Map _map = new Map();
        Exporter _exporter = new Exporter();

        Image curImg = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void newProjectButton_Click(object sender, EventArgs e)
        {
            newProjectWindow newProjWin = new newProjectWindow(ref _map);
            newProjWin.Show();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveWin = new SaveFileDialog();
            saveWin.Filter = "MAPMAKER File|*.mmkr";
            saveWin.Title = "Save Project";
            saveWin.ShowDialog();
            if (saveWin.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveWin.OpenFile();

                // code goes here to place data into file

                fs.Close();
            }
        }

        private void importTileButton_Click(object sender, EventArgs e)
        {
            ImportObjWindow ImportWin = new ImportObjWindow();
            ImportWin.Show();
        }

        private void importTileSetButton_Click(object sender, EventArgs e)
        {
            ImportTileSet ImportWin = new ImportTileSet();
            ImportWin.Show();
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            if (_map._mapName != null)
            {
                SaveFileDialog exportWin = new SaveFileDialog();
                exportWin.Filter = "textfile|*.txt";
                exportWin.Title = "Export Project";
                exportWin.ShowDialog();

                if (exportWin.FileName != "")
                {
                    System.IO.FileStream fs = (System.IO.FileStream)exportWin.OpenFile();

                    _exporter.ExportMap(fs, _map);

                    fs.Close();
                }
            }
            else
            {
                MessageBox.Show("There is no project to export");
            }
        }

        public void makeGrid()
        {
            //Reinitialize things to set display
            MapGrid.Visible = true;
            MapGrid.Width = 0;
            MapGrid.Height = 0;
            MapGrid.ColumnCount = 0;
            MapGrid.RowCount = 0;
            MapGrid.ColumnStyles.Clear();
            MapGrid.RowStyles.Clear();
            MapGrid.Controls.Clear();

            //Get width and height
            int w = _map._mapWidth;
            int tw = _map._tileWidth;
            int h = _map._mapHeight;
            int th = _map._tileHeight;

            //Set scrollbar values
            int vertScrollWidth = SystemInformation.VerticalScrollBarWidth;
            int horScrollHeight = SystemInformation.HorizontalScrollBarHeight;

            if (w * tw < mainPanel.Width && h * th < mainPanel.Height)
            {
                MapGrid.Padding = new Padding(0, 0, vertScrollWidth, horScrollHeight);
            }
            else if (h * th < mainPanel.Height)
            {
                MapGrid.Padding = new Padding(0, 0, 0, horScrollHeight);
            }
            else if (w * tw < mainPanel.Width)
            {
                MapGrid.Padding = new Padding(0, 0, vertScrollWidth, 0);
            }
            
            //Display properly sized tile sections.
            for (int i = 0; i < w; i++)
            {
                MapGrid.ColumnCount++;
                MapGrid.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, tw));
                
            }
            for (int i = 0; i < h; i++)
            {
                MapGrid.RowCount++;
                MapGrid.RowStyles.Add(new RowStyle(SizeType.Absolute, th));
            }

            //Set picture boxes inside the tablelayoutpanel
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    PictureBox p = new PictureBox() { Dock = DockStyle.Fill, Margin = new Padding(0), Location = new Point(i * tw, j * th) };
                    MapGrid.Controls.Add(p);
                    p.Click += new EventHandler(p_Click);
                }
            }
        }
        
        //When a square is clicked, put the currently selected tile in it.
        private void p_Click(object sender, EventArgs e)
        {
            Control c = sender as Control;
            c.BackgroundImage = curImg;
            TableLayoutPanelCellPosition p = MapGrid.GetPositionFromControl(c);
            _map._tileImages[p.Row][p.Column] = curImg;
        }

        //TEMPORARY - select the image (tile) to place.
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            curImg = pictureBox1.Image;
        }

        
    }
}
