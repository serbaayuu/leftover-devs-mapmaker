﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class ImportTileSet : Form
    {

        Bitmap pic;
        public ImportTileSet()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pic = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Load(openFileDialog1.FileName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int rint = Convert.ToInt32(rowbox.Text);
            int cint = Convert.ToInt32(colbox.Text);
            if (rint > 0 && cint > 0 && pictureBox1.Image != null)
            {
                DisplayTileWindow DisplayWin = new DisplayTileWindow(rint, cint, pic);
                DisplayWin.Show();
 
            }
        }
    }
}
