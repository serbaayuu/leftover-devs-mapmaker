﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using TestProgram.Layers;

namespace TestProgram
{
    public class Map
    {
        public int _tileWidth;
        public int _tileHeight;
        public int _mapWidth;
        public int _mapHeight;
        public string _mapName;

        public List<List<Image>> _tileImages;

        //The tileset broken up into a list of tiles. There is only one tileset per map.
        List<Tile> _tileList;
        //The list of layers in the project. The index of the layer indicates draw priority (0 is the bottom most layer)
        public List<Layer> _layers;

        Image _tilesetImage;

        Tile selectedTile;

        public void Initialize(int tileWidth, int tileHeight, int mapWidth, int mapHeight, string mapName)
        {
            _tileWidth = tileWidth;
            _tileHeight = tileHeight;
            _mapWidth = mapWidth;
            _mapHeight = mapHeight;
            _mapName = mapName;

            _layers = new List<Layer>();
            _tileList = new List<Tile>();
            _tileImages = new List<List<Image>>();
            for (int i = 0; i < mapWidth; i++)
            {
                List<Image> l = new List<Image>();
                for (int j = 0; j < mapHeight; j++)
                {
                    Image m = null;
                    l.Add(m);
                }
                _tileImages.Add(l);
            }
            
            //Thisis for testing purposes
            CreateNewTileLayer("tester");
            CreateNewObjectLayer("objectTestLayer");
            //CreateNewRegionLayer("regionLayerTest");

        }

        void SetTileList(Image tileset)
        {
            _tilesetImage = tileset;
            int tileColumns = _tilesetImage.Width / _tileWidth;
            int tileRows = _tilesetImage.Height / _tileHeight;
        }

        void CreateNewTileLayer(string name)
        {
            TileLayer newTileLayer = new TileLayer();
            newTileLayer.Initialize(Layer.LayerType.TileLayer, name, _mapHeight, _mapWidth);
            _layers.Add(newTileLayer);
        }
        void CreateNewObjectLayer(string name)
        {
            ObjectLayer newObjectLayer = new ObjectLayer();
            newObjectLayer.Initialize(Layer.LayerType.ObjectLayer, name, _mapHeight, _mapWidth);

            Obj tempObj1 = new Obj("tempObj1", 30, 40);
            newObjectLayer.AddOject(tempObj1);
            Obj tempObj2 = new Obj("tempObj2", 50, 20);
            newObjectLayer.AddOject(tempObj2);
            Obj tempObj3 = new Obj("tempObj3", 60, 10);
            newObjectLayer.AddOject(tempObj3);
            _layers.Add(newObjectLayer);
        }
        void CreateNewRegionLayer(string name)
        {
            RegionLayer newRegionLayer = new RegionLayer();
            newRegionLayer.Initialize(Layer.LayerType.RegionLayer, name, _mapWidth, _mapHeight);

            Region tempRegion1 = new Region("tempReg1", 40, 60, 30, 30);
            newRegionLayer.AddRegion(tempRegion1);
            _layers.Add(newRegionLayer);
        }
    }
}
