﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TestProgram
{
    class Exporter
    {
        public void ExportMap(System.IO.FileStream fileStream, Map map)
        {
            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                writer.WriteLine("[Properties]");

                writer.WriteLine("MapName=" + map._mapName);
                writer.WriteLine("MapWidth=" + map._mapWidth);
                writer.WriteLine("MapHeight=" + map._mapHeight);
                writer.WriteLine("TileWidth=" + map._tileWidth);
                writer.WriteLine("TileHeight=" + map._tileHeight);
                writer.WriteLine("");

                for (int i = 0; i < map._layers.Count; i++)
                {
                    if (map._layers[i]._layerType == Layers.Layer.LayerType.TileLayer)
                    {
                        writer.WriteLine("[Layer]");
                        writer.WriteLine("Type=Tile");
                        writer.WriteLine("Name=" + map._layers[i]._layerName);

                        for (int x = 0; x < map._mapWidth; x++)
                        {
                            for (int y = 0; y < map._mapHeight; y++)
                            {
                                writer.Write(map._layers[i]._tileGrid[x, y]._tileNumber + ",");
                            }
                            writer.WriteLine("");
                        }
                        writer.WriteLine("");
                    }
                    else if (map._layers[i]._layerType == Layers.Layer.LayerType.ObjectLayer)
                    {
                        writer.WriteLine("[Layer]");
                        writer.WriteLine("Type=Object");
                        writer.WriteLine("Name=" + map._layers[i]._layerName);
                        for (int x = 0; x < map._layers[i]._objectList.Count; x++)
                        {
                            writer.Write(map._layers[i]._objectList[x]._name + ", (" + 
                                map._layers[i]._objectList[x]._xPos + "," + 
                                map._layers[i]._objectList[x]._yPos + "), [properties]=");
                            for (int y = 0; y < map._layers[i]._objectList[x]._propertiesList.Count; y++)
                            {
                                writer.Write(map._layers[i]._objectList[x]._propertiesList[y] + ",");
                            }
                            writer.WriteLine("");
                        }
                    }
                    else if (map._layers[i]._layerType == Layers.Layer.LayerType.RegionLayer)
                    {
                        writer.WriteLine("[Layer]");
                        writer.WriteLine("Type=Region");
                        writer.WriteLine("Name=" + map._layers[i]._layerName);
                        for (int x = 0; x < map._layers[i]._regionList.Count; x++)
                        {
                            writer.Write(map._layers[i]._regionList[x]._name + ", (" +
                                map._layers[i]._regionList[x]._xPos + "," +
                                map._layers[i]._regionList[x]._yPos + "," + 
                                map._layers[i]._regionList[x]._width + "," + 
                                map._layers[i]._regionList[x]._height + "), [properties]=");
                            for (int y = 0; y < map._layers[i]._regionList[x]._propertiesList.Count; y++)
                            {
                                writer.Write(map._layers[i]._regionList[x]._propertiesList[y] + ",");
                            }
                            writer.WriteLine("");
                        }
                    }
                }
            } 
        }
    }
}
