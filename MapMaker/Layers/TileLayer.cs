﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    class TileLayer : Layer
    {
        public override void Initialize(LayerType layerType, string name, int mapHeight, int mapWidth)
        {
            _layerName = name;
            _layerType = layerType;

            _tileGrid = new Tile[mapHeight, mapWidth];
            for (int x = 0; x < mapHeight; x++)
            {
                for (int y = 0; y < mapWidth; y++)
                {
                    Tile newTile = new Tile(0, x, y);
                    _tileGrid[x, y] = newTile;
                }
            }
        }

        public void SetTile(int xPos, int yPos, Tile tile)
        {
            _tileGrid[xPos, yPos] = tile;
        }

        public void DeleteTile(int xPos, int yPos)
        {
            _tileGrid[xPos, yPos] = new Tile(0, xPos, yPos);
        }
    }
}
