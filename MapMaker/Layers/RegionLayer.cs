﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    class RegionLayer : Layer
    {
        public override void Initialize(LayerType layerType, string name, int width, int height)
        {
            _layerType = layerType;
        }

        public void AddRegion(Region reg)
        {
            _regionList.Add(reg);
        }

        public void RemoveRegion(Region reg)
        {
            for (int i = 0; i < _regionList.Count; i++)
            {
                if (_regionList[i]._name == reg._name)
                {
                    _regionList.RemoveAt(i);
                }
            }
        }
    }
}
