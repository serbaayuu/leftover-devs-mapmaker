﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    class ObjectLayer : Layer
    {
        public override void Initialize(LayerType layerType, string name, int mapHeight, int mapWidth)
        {
            _layerName = name;
            _layerType = layerType;
            _objectList = new List<Obj>();
        }

        public void AddOject(Obj obj)
        {
            _objectList.Add(obj);
        }

        public void RemoveObject(Obj obj)
        {
            for (int i = 0; i < _objectList.Count; i++)
            {
                if (_objectList[i]._name == obj._name)
                {
                    _objectList.RemoveAt(i);
                }
            }
        }
    }
}
