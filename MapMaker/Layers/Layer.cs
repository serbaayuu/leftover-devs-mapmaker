﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram.Layers
{
    public class Layer
    {
        public enum LayerType
        {
            TileLayer = 0,
            ObjectLayer = 1,
            RegionLayer = 2,
        }
        public LayerType _layerType;

        public string _layerName;

        //Used only in Tile Layers
        public Tile[,] _tileGrid;
        //Used only in Object Layers
        public List<Obj> _objectList;
        //used only in regions
        public List<Region> _regionList;

        public virtual void Initialize(LayerType layerType, string name, int mapHeight, int mapWidth)
        {
            
        }
    }
}
