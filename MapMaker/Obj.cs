﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TestProgram
{
    public struct Obj
    {
        public string _name;
        public List<string> _propertiesList;
        public int _xPos;
        public int _yPos;

        Image _tileImage;

        //constructs a new tile
        public Obj(string name, int xpos, int ypos, Image tileImage = null)
        {
            _name = name;
            _tileImage = tileImage;
            _propertiesList = new List<string>();
            _xPos = xpos;
            _yPos = ypos;
        }

        public void AddProperty(string newProperty)
        {
            _propertiesList.Add(newProperty);
        }

        public void RemoveProperty(string propertyToRemove)
        {
            for (int i = 0; i < _propertiesList.Count; i++)
            {
                if (_propertiesList[i] == propertyToRemove)
                {
                    _propertiesList.RemoveAt(i);
                }
            }
        }
    }
}
