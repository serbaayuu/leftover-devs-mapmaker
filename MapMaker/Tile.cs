﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TestProgram
{
    public struct Tile
    {
        public const int WIDTH = 32;
        public const int HEIGHT = 32;
        public List<string> _propertiesList;

        //0 indicates blank tile
        public int _tileNumber;

        //the x,y position of the tile on the tileset
        int _xPos;
        int _yPos;
        Image _tileImage;

        //constructs a new tile
        public Tile(int tileNumber, int xpos, int ypos, Image tileImage = null)
        {
            _xPos = xpos;
            _yPos = ypos;
            _tileImage = tileImage;

            this._tileNumber = tileNumber;
            _propertiesList = new List<string>();
        }

        public void AddProperty(string newProperty)
        {
            _propertiesList.Add(newProperty);
        }

        public void RemoveProperty(string propertyToRemove)
        {
            for (int i = 0; i < _propertiesList.Count; i++)
            {
                if (_propertiesList[i] == propertyToRemove)
                {
                    _propertiesList.RemoveAt(i);
                }
            }
        }
    }
}
