﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProgram
{
    public struct Region
    {
        public string _name;
        public List<string> _propertiesList;
        public int _xPos;
        public int _yPos;
        public int _width;
        public int _height;

        //constructs a new Region
        public Region(string name, int xpos, int ypos, int width, int height)
        {
            _name = name;
            _propertiesList = new List<string>();
            _xPos = xpos;
            _yPos = ypos;
            _width = width;
            _height = height;
        }

        public void AddProperty(string newProperty)
        {
            _propertiesList.Add(newProperty);
        }

        public void RemoveProperty(string propertyToRemove)
        {
            for (int i = 0; i < _propertiesList.Count; i++)
            {
                if (_propertiesList[i] == propertyToRemove)
                {
                    _propertiesList.RemoveAt(i);
                }
            }
        }

    }
}
