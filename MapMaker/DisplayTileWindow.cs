﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProgram
{
    public partial class DisplayTileWindow : Form
    {
        int x = 0;
        int y = 0;
        int tileh;
        int tilew;
        Rectangle cloneRect;

        public DisplayTileWindow(int r, int c, Bitmap pic)
        {
            InitializeComponent();
            tileh = pic.Height / r;
            tilew = pic.Width / c;
            cloneRect = new Rectangle(0, 0, tilew, tileh);
            TileSplit(r, c, pic);
        }

        void TileSplit(int rows, int col, Bitmap map)
        {
            TableLayoutPanel tileholder = new TableLayoutPanel();
            tileholder.Location = new Point(10, 40);
            tileholder.Dock = DockStyle.Fill;
            tileholder.ColumnCount = col;
            tileholder.AutoScroll = true;
            tileholder.RowCount = 0;
            tileholder.MaximumSize = new Size(map.Width, map.Height);
            tileholder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            tileholder.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddRows;
            this.Controls.Add(tileholder);
            int rowtrack = 0;
            Bitmap cloneBitmap;
            System.Drawing.Imaging.PixelFormat format = map.PixelFormat;
            for (int z = 0; z < rows; z++)
            {
                for (int i = 0; i < col; i++)
                {
                    cloneRect = new Rectangle(tilew * i, tileh * z, tilew, tileh);
                    //MessageBox.Show(tileh.ToString(), "My Application", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    PictureBox p1 = new PictureBox();
                    p1.Dock = DockStyle.Fill;
                    p1.SizeMode = PictureBoxSizeMode.AutoSize;
                    cloneBitmap = map.Clone(cloneRect, format);
                    p1.Image = cloneBitmap;
                    tileholder.Controls.Add(p1, i, rowtrack);
                }

                tileholder.RowStyles.Add(new RowStyle(SizeType.Absolute, tileh));


                for (int j = 0; j < col; j++)
                {
                    Panel n1 = new Panel();
                    n1.Dock = DockStyle.Fill;

                    TextBox t1 = new TextBox();
                    t1.Height = 20;
                    t1.Width = 30;
                    n1.Controls.Add(t1);
                    tileholder.Controls.Add(n1, j, rowtrack + 1);
                }
                rowtrack = rowtrack + 2;
                tileholder.RowStyles.Add(new RowStyle(SizeType.Absolute, 30)); // 30 is the rows space
            }

        }
    }
}
